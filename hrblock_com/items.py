# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import re
import scrapy
from scrapy.loader.processors import Join, TakeFirst, Compose, MapCompose


def filter_nonalpha(x):
    return x if not re.match('^[,\s]*$', x) else None


class HrblockComItem(scrapy.Item):
    Name = scrapy.Field(
        output_processor=TakeFirst(),
    )
    Address = scrapy.Field(
        input_processor=MapCompose(filter_nonalpha),
        output_processor=Join(separator=', '),
    )
    City = scrapy.Field(
        output_processor=TakeFirst(),
    )
    State = scrapy.Field(
        output_processor=TakeFirst(),
    )
    Country = scrapy.Field(
        output_processor=TakeFirst(),
    )
    FAXPhone = scrapy.Field(
        output_processor=TakeFirst(),
    )
    TAXPhone = scrapy.Field(
        output_processor=TakeFirst(),
    )
    Zipcode = scrapy.Field(
        output_processor=TakeFirst(),
    )
    Authorized = scrapy.Field(
        output_processor=TakeFirst(),
    )
    Dealer = scrapy.Field(
        output_processor=TakeFirst(),
    )
    Url = scrapy.Field(
        output_processor=TakeFirst(),
    )